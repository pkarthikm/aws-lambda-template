package com.zsassociates.platformfoundation.template;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.zsassociates.platformfoundation.template.util.DatabaseConnectionHelper;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class LambdaRequestStreamHandler implements RequestStreamHandler {

    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {
        LambdaLogger logger = context.getLogger();
        String input = IOUtils.toString(inputStream, "UTF-8");
        logger.log("Input: " + input);
        logger.log("Function: " + context.getFunctionName());
        String currentTime = "unavailable";
        try {

            Connection conn = DatabaseConnectionHelper.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet resultSet = stmt.executeQuery("SELECT NOW()");

            if (resultSet.next()) {
                currentTime = resultSet.getObject(1).toString();
            }

            logger.log("Successfully executed query.  Result: " + currentTime);

        } catch (Exception e) {
            e.printStackTrace();
            logger.log("Caught exception: " + e.getMessage());
        }
        outputStream.write(("Hello World - " + input + " Time from DB: " + currentTime).getBytes());
    }
}

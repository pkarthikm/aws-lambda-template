package com.zsassociates.platformfoundation.template.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnectionHelper {
    private static DatabaseConnectionHelper instance = null;
    private final Connection connection;

    private DatabaseConnectionHelper() throws IOException, SQLException {
        ApplicationProperties dsProps = new ApplicationProperties("datasource.properties");
        connection = DriverManager.getConnection(
                dsProps.getProperty("url"),
                dsProps.getProperty("username"),
                dsProps.getProperty("password"));
    }
    public static Connection getConnection() throws SQLException, IOException {
        if(null == instance || null == instance.connection) {
            instance = new DatabaseConnectionHelper();
        }
        return instance.connection;
    }

}

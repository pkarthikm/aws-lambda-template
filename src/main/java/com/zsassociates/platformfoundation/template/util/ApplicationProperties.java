package com.zsassociates.platformfoundation.template.util;

import com.amazonaws.util.StringUtils;
import sun.swing.StringUIClientPropertyKey;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationProperties {
    private final Properties properties = new Properties();

    public ApplicationProperties(String propertyFile) throws IOException {
        if(StringUtils.isNullOrEmpty(propertyFile))
            throw new IllegalArgumentException();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertyFile);
        if(null == inputStream)
            throw new FileNotFoundException("Property File not found in the class path");
        this.properties.load(inputStream);
    }

    public String getProperty(String key) throws IllegalArgumentException{
        if(StringUtils.isNullOrEmpty(key))
            throw new IllegalArgumentException();
        return properties.getProperty(key);
    }
}

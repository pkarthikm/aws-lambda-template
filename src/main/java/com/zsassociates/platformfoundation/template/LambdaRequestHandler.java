package com.zsassociates.platformfoundation.template;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.zsassociates.platformfoundation.template.util.DatabaseConnectionHelper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class LambdaRequestHandler implements RequestHandler<String, String> {
    public String handleRequest(String input, Context context) {
        LambdaLogger logger = context.getLogger();
        logger.log("Input: " + input);
        logger.log("Function: " + context.getFunctionName());
        String currentTime = "unavailable";
        try {
            Connection conn = DatabaseConnectionHelper.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet resultSet = stmt.executeQuery("SELECT NOW()");

            if (resultSet.next()) {
                currentTime = resultSet.getObject(1).toString();
            }
            logger.log("Successfully executed query.  Result: " + currentTime);

        } catch (Exception e) {
            e.printStackTrace();
            logger.log("Caught exception: " + e.getMessage());
        }
        return "Hello World - " + input + " Time from DB: " + currentTime;
    }
}
